# Java3ProjectTestGenerator
This function as a test generator and grader. This generates test given a file input with specifically formatted strings, as well as grading a test given a list of answers (the student's "answer sheet")

## Running the program

Under the file JavaFX.java with the main method inside the folder javaFX, run this to start the GUI.

## Generating Tests
In order to generate the exam, you need to provide an input and output file.
You must provide paths to those files.


_Input file:_ Provide a file with lines of strings to be parsed. Each line will become a question on the exam. The lines must be formatted in a certain way (see table)
_Output file:_ The exam will be generated and output to this file.

| Question type | String format | other notes |
| ------ | ------ | ------ |
| Simple question ans answer | simplequestion\|\<question text\>\|\<correct answer\> | This is strictly graded, the answer student writes must exactly match the expected ansswer. |
| Fill in the blank | fillblankquestion\|\<question text\>\|\<correct answer\> | The question text MUST contain the right answer (to be replaced with a blank)
| Multiple choice | multiplechoicequestion\|\<question text\>\|\<answerA>,\<answerB\>..\|\<correct answer\> | **MIN 2 / MAX 5 answer choices.** Answer choices must contain the correct answer | The student must provide the answer text, not including the answer letter |
| Math Linear |\<math-linear\>\|\<question goal>|Generate a random math linear equation (y = mx + b) for a random line. Possible questionGoals: **"solvex"** - solve x given y, **"findslope"** - Find slope given a table of points. NOTE: (default will generate "findslope" if an invalid goal given) | 
| Math Linear (CUSTOM RANGE) |\<math-linear\>\|\<question goal>\|\<xrangesize\>\|\<xrangelowerbound\>| SEE above notes for Math Linear. You can also have a format where you specify how many possible x values in a range, as well as a lowerBound (minimum x value where the range starts). **NOTE: the size of the range must be positive.**  | 
| Math Parabola |\<math-parabola\>\|\<question goal>|Generate a random parabola equation to solve, defined by the vertex form equation y = a(x - h)^2 + k. Possible questionGoals: **"solvey"** - solve y given x, **"findparametera"** - Determine the a parameter (parabola stretch/narrow factor) given points. NOTE: (default will generate "findparametera" if an invalid goal given) | 

### Sample Question Strings:

fillblankquestion|Dan is the best teacher for Programming.|best<br>
multiplechoicequestion|Which of these is not a duelist?|Jett,Sage,Phoenix,Yoru|Sage<br>
simplequestion|What is the opposite of true?|false<br>
math-linear|solvex<br>
math-linear|solvex|10|-5<br>
math-linear|other<br>
math-parabola|solvey<br>
math-parabola|findparametera<br>

## Grading a Test
In order to grade the exam, you need to provide an input and output file.
You must provide paths to those files.

_Input file:_ Provide a file with lines of strings of the student answers. The test is strictly graded. Note: Each line of the file is an answer. If the file has more answer lines than the total number of questions on the test, the program will ignore the extra answers. If the student wrote less lines than the total number of questions, the program will only scan the answered questions.
_Output file:_ The exam along with the student answers will be output along with the resulting outcome (correct/incorrect) and a total score.

### Sample ANSWER SHEET (1 LINE, 1 ANSWER):

best<br>
Sage<br>
false<br>
11<br>
43<br>
-5<br>
2<br>
1<br>
