package src.eventHandlers;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import src.AllExams;
import src.TestGenerator;
import src.javaFX.*;

/**
 * Handles the event fired when create button in the main pane (CreateExamFX) is clicked
 * @author Adam
 */
public class CreateExamHandler implements EventHandler<ActionEvent> {

    private CreateExamFX mainPane;
    private StoredExamsPaneFX sideBar;
    private AllExams allExams;

    public CreateExamHandler(CreateExamFX mainPane, StoredExamsPaneFX sideBar, AllExams allExams) {
        this.mainPane = mainPane;
        this.sideBar = sideBar;
        this.allExams = allExams;
    }

    @Override
    public void handle(ActionEvent event) {
        TestGenerator tg = null;
        try {
            tg = new TestGenerator(mainPane.getInputPath().getText(), mainPane.getOutputPath().getText());
            allExams.addExam(mainPane.getExamName().getText(), tg.getQuestions());

            StoredExamFX storedExamFX = new StoredExamFX(mainPane.getExamName().getText());
            sideBar.addStoredExam(storedExamFX);

            setEventHandlers(storedExamFX);

            mainPane.getMessage().setText("All Good!");
        } catch (IllegalArgumentException e) {
            // TODO: handle exception
            mainPane.getMessage().setText(e.getMessage());
        }
    }

    /**
     * Sets the event handlers for recreateExamBtn and gradeExamBtn in a storedExamFX
     * @param storedExamFX
     */
    private void setEventHandlers(StoredExamFX storedExamFX) {
        storedExamFX.getRecreateExamBtn().setOnAction(new RecreateExamHandler(storedExamFX.getRecreateOutputPath(),
                allExams.getExam(sideBar.getExamCount() - 1), storedExamFX.getRecreateMessage()));
        storedExamFX.getGradeExamBtn().setOnAction(
                new GradeExamHandler(storedExamFX.getGradeInputPath(), storedExamFX.getGradeOutputPath(),
                        allExams.getExam(sideBar.getExamCount() - 1), storedExamFX.getGradeMessage()));
    }
}
