package src.eventHandlers;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;
import src.Exam;
import src.TestGrader;

/**
 * Handles the event fired when grade button in the Stored exams pane is clicked
 * @author Adam
 */
public class GradeExamHandler implements EventHandler<ActionEvent>{
    
    private TextField gradeInputPath;
    private TextField gradeOutputPath;
    private Exam exam;
    private Text gradeMessage;

    public GradeExamHandler(TextField gradeInputPath, TextField gradeOutputPath, Exam exam, Text gradeMessage){
        this.gradeInputPath = gradeInputPath;
        this.gradeOutputPath = gradeOutputPath;
        this.exam = exam;
        this.gradeMessage = gradeMessage;

    }

    @Override
    public void handle(ActionEvent event) {
        try {
            new TestGrader(this.gradeInputPath.getText(), this.gradeOutputPath.getText(), this.exam);
            this.gradeMessage.setText("All Good!");
        } catch (IllegalArgumentException e) {
            //TODO: handle exception
            this.gradeMessage.setText(e.getMessage());
        }
                 
    } 
}
