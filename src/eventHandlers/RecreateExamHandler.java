package src.eventHandlers;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;
import src.Exam;
import src.TestGenerator;

/**
 * Handles the event fired when recreate button in the Stored exams pane is clicked
 * @author Adam
 */
public class RecreateExamHandler implements EventHandler<ActionEvent>{

    private TextField recreateOutputPath;
    private Exam exam;
    private Text recreateMessage;

    public RecreateExamHandler(TextField recreateOutputPath, Exam exam, Text recreateMessage){
        this.recreateOutputPath = recreateOutputPath;
        this.exam = exam;
        this.recreateMessage = recreateMessage;

    }

    @Override
    public void handle(ActionEvent event) {
        try {
            new TestGenerator(this.exam, this.recreateOutputPath.getText()); 
            this.recreateMessage.setText("All Good!");
        } catch (IllegalArgumentException e) {
            //TODO: handle exception
            this.recreateMessage.setText(e.getMessage());
        }
                   
    }  
}
