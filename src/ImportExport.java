package src;

import java.io.IOException;
import java.util.List;


/**
 * An interface for Import and exporting file classes. The class FileInOut implemented this.
 * @author Adam
 */
public interface ImportExport {

    List<String> importFile() throws IOException;

    void writeListToFile(List<String> lines) throws IOException;

    void writeStringToFile(String lines) throws IOException;
}
