package src;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

/**
 * This class handles the parsing of strings. Each method is responsible of parsing specific string formats which determined
 * the type of question to be created
 * @author Adam, Kyle
 * 
 */
public class QuestionAnswerParsers {

    private static final int QUESTION_TEXT_COLUMN = 1;

    public static Question parseMultipleChoice(String qString){
        final int ANSWER_CHOICES_COLUMN = 2;
        final int CORRECT_ANSWER_COLUMN = 3;
        final int FORMAT_SPLIT_LENGTH = 4;  //the length of the split array (# of pieces expected)
        
        
        String[] strings = qString.split("\\|");
        
        //check if the length of the resulting split strings array is equal to the correct length given the format
        if (strings.length != FORMAT_SPLIT_LENGTH) {
            throw new IllegalArgumentException("Error in string format!");
        }

        if (!(validateMCFormat(strings[ANSWER_CHOICES_COLUMN].split(","), strings[CORRECT_ANSWER_COLUMN]))) {
            throw new IllegalArgumentException("Possible answer range must be between 2 to 5 as well as the possible answers must contain the correct answer!");
        }
        List<String> possibleAnswers = Arrays.asList(strings[ANSWER_CHOICES_COLUMN].split(",")); 

        Question question = new MultipleChoiceQuestion(strings[QUESTION_TEXT_COLUMN], strings[CORRECT_ANSWER_COLUMN], possibleAnswers);
        return question;
    }

    public static Question parseFillBlank(String qString){
        if (!validateFillBlank(qString)) {
            throw new IllegalArgumentException("fillblankquestion format is invalid");
        }
        
        final int INDEX_QUESTION = 1;
        final int INDEX_ANSWER = 2;

        String[] strings = qString.split("\\|");

        Question question = new FillBlankQuestion(strings[INDEX_QUESTION], strings[INDEX_ANSWER]);

        return question;
    }

    public static Question parseSimpleQuestion(String qString){
        if (!validateSimpleQuestion(qString)) {
            throw new IllegalArgumentException("SimpleQuestion format is invalid");
        }

        final int INDEX_QUESTION = 1;
        final int INDEX_ANSWER = 2;
        String[] strings = qString.split("\\|");

        Question question = new SimpleQuestion(strings[INDEX_QUESTION], strings[INDEX_ANSWER]);

        return question;
    }

    // format: fillblankquestion|Dan is the best teacher for Programming.|best
    private static boolean validateFillBlank(String qString){
        final int INDEX_QUESTION = 1;
        final int INDEX_ANSWER = 2;

        //checkif its the same format
        if (!validateSimpleQuestion(qString)) {
            return false;
        }
        String[] strings = qString.split("\\|");


        //invalid if the answer isnt one of the list of possible answers
        if(!strings[INDEX_QUESTION].contains(strings[INDEX_ANSWER])){
            return false;
        }

        return true;
    }

    private static boolean validateSimpleQuestion(String qString){
        final int VALID_NUMBER_PIPES = 2;
        final int VALID_NUMBER_ARRAY_ELEMENTS = 3;
        int count = 0;
        for(char c : qString.toCharArray()){
            if(c == '|'){
                count++;
            }
        }
        if(count != VALID_NUMBER_PIPES){
            return false;
        }
    
        String[] strings = qString.split("\\|");
        if(strings.length != VALID_NUMBER_ARRAY_ELEMENTS){
            return false;
        }

        return true;
    }

    //these are for parsing linear equations 

    //parse linearQuestionString without specified x range and lowerbound
    public static Question parseMathLinearQuestion(String qString) {
        Random rng = new Random();
        String[] strings = qString.split("\\|");
        final int FORMAT_SPLIT_LENGTH = 2; 

        if (strings.length != FORMAT_SPLIT_LENGTH) {
            throw new IllegalArgumentException("Error in string format.");
        }

        MathGenerator mathGenerator = new LinearEquationGenerator(strings[QUESTION_TEXT_COLUMN], rng);
        Question generatedMathQuestion = mathGenerator.createMathematicalQuestion();
        return generatedMathQuestion;
    }

    //parse linearQuestionString with specified x range and lowerbound
    public static Question parseMathLinearQuestionCustomRange(String qString) {

        final int RANGE_SIZE_COLUMN = 2;
        final int MIN_X_COLUMN = 3;
        final int FORMAT_SPLIT_LENGTH = 4; 

        Random rng = new Random();
        String[] strings = qString.split("\\|");

        int xRangeSize = Integer.parseInt(strings[RANGE_SIZE_COLUMN]);
        int minX = Integer.parseInt(strings[MIN_X_COLUMN]);

        if (strings.length != FORMAT_SPLIT_LENGTH) {
            throw new IllegalArgumentException("Error in string format.");
        }

        if (!validateRangeSize(strings, xRangeSize)) {
            throw new IllegalArgumentException("range of possible x values must be 1 or greator!");
        }
        MathGenerator mathGenerator = new LinearEquationGenerator(strings[QUESTION_TEXT_COLUMN], rng, xRangeSize, minX);
        Question generatedMathQuestion = mathGenerator.createMathematicalQuestion();
        return generatedMathQuestion;
    }

    //parse Parabola (format: math-parabola|<questiongoal>)
    public static Question parseMathParabolaQuestion(String qString) {
        Random rng = new Random();
        String[] strings = qString.split("\\|");
        final int FORMAT_SPLIT_LENGTH = 2; 

        if (strings.length != FORMAT_SPLIT_LENGTH) {
            throw new IllegalArgumentException("Error in string format.");
        }

        //strings[QUESTION_TEXT_COLUMN] will contain the questiongoal
        MathGenerator mathGenerator = new Parabola(strings[QUESTION_TEXT_COLUMN], rng);
        Question generatedMathQuestion = mathGenerator.createMathematicalQuestion();
        return generatedMathQuestion;
    }

    //validates the string format for multiple choice questions
    private static boolean validateMCFormat(String[] possibleAnswers, String correctAnswer) {

        final int MAX_NUM_CHOICES = 5;
        final int MIN_NUM_CHOICES = 2;

        //the array (as an ArrayList) of possible answers must contain the conrect answer
        if (!Arrays.asList(possibleAnswers).contains(correctAnswer)) {
            System.out.println("does not contain real answer!");
            return false;
        }

        //number of possible answers must be between MAX_NUM_CHOICES and MIN_NUM_CHOICES.
        if (possibleAnswers.length < MIN_NUM_CHOICES || possibleAnswers.length > MAX_NUM_CHOICES) {
            System.out.println("outside range!");
            return false;
        }
        return true;
    }

    //the range size of possible x values must be 1 or greator
    private static boolean validateRangeSize(String[] strings, int xRangeSize) {
        return xRangeSize >= 1;
    }
    
}
