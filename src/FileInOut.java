package src;
import java.io.IOException;
import java.nio.file.*;
import java.util.List;

/**
 * This class is responsible of reading input files and writing to output files.
 * @author Adam
 */
public class FileInOut implements ImportExport{
    private Path importPath;
    private Path exportPath;

    public FileInOut(String importPathString, String exportPathString){
        this.importPath = Paths.get(importPathString);
        this.exportPath = Paths.get(exportPathString);

    }

    //This will return all the contents of the file in a list of strings
    public List<String> importFile() throws IOException{
        return Files.readAllLines(this.importPath);
    }


    //this will write a given list of strings (lines of text) to an output file
    public void writeListToFile(List<String> lines) throws IOException{
        StringBuilder builder = new StringBuilder();
        for (String string : lines) {
            builder.append(string + "\n");
        }
        Files.write(this.exportPath, builder.toString().getBytes());

    }

    //writes one large string (can have new lines) to file
    public void writeStringToFile(String lines) throws IOException{
        Files.write(this.exportPath, lines.getBytes());

    }

    //getters/setters
    // public Path getImportPath() {
    //     return importPath;
    // }

    // public void setImportPath(Path path) {
    //     this.importPath = path;
    // }

    // public Path getExportPath() {
    //     return exportPath;
    // }

    // public void setExportPath(Path exportPath) {
    //     this.exportPath = exportPath;
    // }
    
}