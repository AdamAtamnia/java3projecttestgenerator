package src;
import java.io.IOException;
import java.nio.file.NoSuchFileException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;



/**
 * This class is reponsible for grading an exam given an answer sheet (a file), where it will output the results
 * to a file with a grade.
 * @author Kyle
 */

public class TestGrader {


    private List<String> studentAnswers;
    private int numberAnswered;
    private int numberCorrect;

    //File input will only have student answers, in order.
    
    public TestGrader(String inFilePath, String outFilePath, Exam test) {

        Pattern pattern = Pattern.compile(".txt$");
        if (!pattern.matcher(inFilePath).find() || !pattern.matcher(outFilePath).find()) {
            throw new IllegalArgumentException("Error, all files have to be .txt, try again!");
        }
        
        FileInOut fileInOut = new FileInOut(inFilePath, outFilePath);
        this.numberCorrect = 0;
        try {
            this.studentAnswers = fileInOut.importFile();
            removeAllEmptyLines();
            this.numberAnswered = studentAnswers.size();
            correctTest(test, fileInOut);

        } catch (NoSuchFileException e) {
            throw new IllegalArgumentException("Grader: file not found, try again!");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //this constructor (directly given a string list of answers instead of a file) is for testing purposes only.
    public TestGrader(List<String> studentAnswers) {
        this.studentAnswers = studentAnswers;
        this.numberCorrect = 0;
        removeAllEmptyLines();
        this.numberAnswered = studentAnswers.size();
        try {
            this.studentAnswers = studentAnswers;

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //getters
    public int getNumberAnswered() {
        return this.numberAnswered;
    }

    public int getNumberCorrect() {
        return this.numberCorrect;
    }

    //corrects the whole test, given input an exam object and a file of the student's answers.
    public void correctTest(Exam test, FileInOut fileInOut) throws IOException {

        //get # questions
        int totalNumberQuestions = test.getNumberQuestions();

        checkNumberAnswers(test);
        StringBuilder sb = new StringBuilder(); //this will contain a string of the exam questions along with answer and outcome
        
        //loop through all the student answers, comparing the answers to the corresponding questions of the test
        //(assuming the size of the arrayList is the number of answered questions out of the total number of questions)
        for (int i = 0; i < this.numberAnswered; i++) {
            String studentAnswer = this.studentAnswers.get(i);
            Question question = test.getQuestion(i);
            //add one point if student gets right answer
            if (correctQuestion(question, studentAnswer)) {
                this.numberCorrect++;
            }
            sb.append(displayCorrectedQuestion(question, studentAnswer));
            
        }
        //finally, display total result to the file and write to the file.
        sb.append("\n=============================" +
                  "\n YOUR SCORE:" + this.numberCorrect + "/" + totalNumberQuestions +
                  "\n=============================");
        fileInOut.writeStringToFile(sb.toString());
    }

    private void checkNumberAnswers(Exam test) {
        //if the student provides more answers(strings) than the questions, set the # answered = to the # of questions of that exam
        if (this.numberAnswered > test.getNumberQuestions()) {
            this.numberAnswered = test.getNumberQuestions();
        }
    }

    //THIS IS USED FOR UNIT TESTING (no file input / output)
    public void correctTest(Exam test, List<String> studentAnswers) {

        //get # questions
        int totalNumberQuestions = test.getNumberQuestions();

        StringBuilder sb = new StringBuilder(); //this will contain a string of the exam questions along with answer and outcome

        checkNumberAnswers(test);
        
        //loop through all the student answers, comparing the answers to the corresponding questions of the test
        //(assuming the size of the arrayList is the number of answered questions out of the total number of questions)
        for (int i = 0; i < this.numberAnswered; i++) {
            String studentAnswer = this.studentAnswers.get(i);
            Question question = test.getQuestion(i);
            //add one point if student gets right answer
            if (correctQuestion(question, studentAnswer)) {
                this.numberCorrect++;
            }
            sb.append(displayCorrectedQuestion(question, studentAnswer));
            
        }
        //finally, display total result to the file and write to the file.
        sb.append("\n=============================" +
                  "\n YOUR SCORE:" + this.numberCorrect + "/" + totalNumberQuestions +
                  "\n=============================");
    }

    

    //this corrects a question given a question on the exam and a student's answer.
    //returns true if right, false if wrong
    public boolean correctQuestion(Question testQuestion, String studentAnswer) {
        Question questionStudentAnswer = testQuestion.addStudentAnswer(testQuestion, studentAnswer);
        return questionStudentAnswer.getAnswer().equals(studentAnswer);
    }

    //return a string with question plus the answer outcomes (correct/incorrect)
    private String displayCorrectedQuestion(Question examQuestion, String studentAnswer) {
        String correctedQuestionString = examQuestion.makeQuestionString() + "\n" + "YOUR ANSWER: " + studentAnswer + " -- ";

        if (correctQuestion(examQuestion, studentAnswer)) {
            correctedQuestionString += "CORRECT";
        } else {
            correctedQuestionString += "INCORRECT: Correct answer: " + examQuestion.getAnswer();
        }
        //append new lines to separate each question
        return correctedQuestionString + "\n\n";
        
    }

 
    //this will remove all empty lines of the student answers (if a student enters empty lines in his answer file)
    private void removeAllEmptyLines() {
        List<String> noEmptyLines = new ArrayList<String>();
        for (String str : this.studentAnswers) {
            if (!str.equals("")) {
                noEmptyLines.add(str);
            }
        }

        this.studentAnswers = noEmptyLines;
    }

}