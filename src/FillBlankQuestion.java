package src;

/**
 * This class is for creating fill in the blank questions. The question text must contain the correct answer, in which
 * the answer in the question text will be replace with a blank.
 * The answer must be exactly written in order to be correct.
 * @author Adam
 * 
 */
public class FillBlankQuestion extends Question {

    public FillBlankQuestion(String question, String answer){
        super(question, answer, null);
    }
    
    public FillBlankQuestion(Question question, String studentAnswer){
        super(question.getQuestion(), question.getAnswer(), studentAnswer);
    }

    @Override
    public String makeQuestionString() {
        String questionString = super.getQuestion();
        String regex = "\\b" + this.getAnswer() + "\\b";     //regex that only matches the answer (full word)

        //replace first answer word with a blank.
        questionString = questionString.replaceFirst(regex, "______");
        return "Fill in the blank:\n" + questionString;
    }


    @Override //testing purposes only
    public String toString() {
        return super.toString();
    }

    @Override
    public Question addStudentAnswer(Question oldQuestion, String studentAnswer) {
        Question withStudentAnswer = new FillBlankQuestion(oldQuestion, studentAnswer);
        return withStudentAnswer;
    }

    
}
