package src;

import java.util.ArrayList;
import java.util.List;


/**
 * Stores All the exam objects
 * @author Adam
 */
public class AllExams {

    List<Exam> Exams = new ArrayList<Exam>();

    public void addExam(String name, List<Question> questions){
        Exam exam = new Exam(name, questions);
        Exams.add(exam);
    }

    public Exam getExam(int index){
        return Exams.get(index);
    }
}
