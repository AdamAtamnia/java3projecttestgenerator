package src;

/**
 * This is an abstract class where classes inheriting will be different types of Questions.
 * @author Adam, Kyle
 * 
 */
public abstract class Question {
    private String question;
    private String answer;
    private String studentAnswer;

    public Question(String question, String answer, String studentAnswer){
        if (question == null || answer == null || question == "" || answer == ""){
            throw new IllegalArgumentException("Question or answer cannot be null or empty");
        }
        this.question = question;
        this.answer = answer;
        this.studentAnswer = studentAnswer;
    }

    // create a new question object when setting a student answer
    public Question(Question question, String studentAnswer){
        this(question.getQuestion(), question.getAnswer(), studentAnswer);
    }

    //the question string (displayed in the test) varies depending on the type of question
    public abstract String makeQuestionString();

    public String getQuestion() {
        return question;
    }

    public String getAnswer() {
        return answer;
    }

    public String getStudentAnswer() {
        return studentAnswer;
    }

    @Override
    public String toString(){
        return "Question: " + this.question + ", Answer: " + this.answer + ", Student Answer: " + this.studentAnswer;
    }

    @Override
    public boolean equals(Object questionObj) {
 
        return this.question.equals(((Question)questionObj).getQuestion()) && 
        this.answer.equals(((Question)questionObj).getAnswer());

    }
    
    public abstract Question addStudentAnswer(Question oldQuestion, String studentAnswer);
    
}