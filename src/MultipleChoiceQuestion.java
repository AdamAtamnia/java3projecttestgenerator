package src;
import java.util.List;

/**
 * This class is for creating Multiple Choice questions. The question will contain a list of possible answers, where one of them
 * must be the correct answer. The student must type the answer (not the answer letter) to be correct.
 * @author Kyle
 */
public class MultipleChoiceQuestion extends Question {

    private List<String> possibleAnswers;
    public MultipleChoiceQuestion(String question, String answer, List<String> possibleAnswers){
        super(question, answer, null);
        
        this.possibleAnswers = possibleAnswers;
    }
    public MultipleChoiceQuestion(Question question, String studentAnswer, List<String> possibleAnswers){
        super(question.getQuestion(), question.getAnswer(), studentAnswer);
        this.possibleAnswers = possibleAnswers;
    }

    public List<String> getPossibleAnswers(){
        return this.possibleAnswers;
    }

    @Override
    public String makeQuestionString() {
        String possibleAnswersList = createAnswerlistString();
        return super.getQuestion() + "\n" + possibleAnswersList;
    }

    @Override
    public String toString() {
        String possibleAnswersList = createAnswerlistString();
        return super.toString() + "\n" + possibleAnswersList;
    }

    @Override
    public boolean equals(Object questionObj) {
        return super.equals(questionObj) && this.possibleAnswers.equals(((MultipleChoiceQuestion)questionObj).getPossibleAnswers());

    }
    

    //returns a string formatted representation of a the list of multiple choice answers.
    private String createAnswerlistString() {
        String possibleAnswersList = "";
        for (int i = 0; i < this.possibleAnswers.size(); i++) {
            possibleAnswersList += (char)('A' + i) + ") " + this.possibleAnswers.get(i);

            //append new line except on the last index of possibleAnswers
            possibleAnswersList += ( i != this.possibleAnswers.size() - 1 ? "\n" : "");
        }
        return possibleAnswersList;
    }

    
    @Override
    public Question addStudentAnswer(Question oldQuestion, String studentAnswer) {
        Question withStudentAnswer = new MultipleChoiceQuestion(oldQuestion, studentAnswer, this.getPossibleAnswers());
        return withStudentAnswer;
    }

}
