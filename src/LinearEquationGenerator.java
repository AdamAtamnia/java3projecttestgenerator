package src;
import java.util.Random;

/**
 * This class is responsible for generating linear equations
 * revolving around y = mx + b. The goal of the question will depend on the input given in the constructor.
 * 
 * ==possible goals==
 * -solvex: solve x given y, 
 * -fildslope (the default): find slope given table of values
 * @author Kyle
 */
public class LinearEquationGenerator extends MathGenerator {


    private int answer;
    private int slope;
    private int yIntercept;

    //how many possible slope / y-intercept values this range spans (ex: from 0 to 16)
    //NOTE: when these values are used, this value is added by 1 since the Random.nextInt's upperbound input is exclusive.
    private static final int SLOPE_RANGE_SIZE = 16; 
    private static final int Y_INT_RANGE_SIZE = 100; 

    //offset values (from 0) to set the lowest slope / y-intercept value possible for the range of slope / intercept values
    private static final int SLOPE_RANGE_OFFSET = -8;
    private static final int Y_INT_RANGE_OFFSET = -50;

    
    //constructor (setting default possible x values for solve-x goal)
    public LinearEquationGenerator(String questionGoal, Random rng) {
        super(questionGoal, rng);

        //set the slope to a random value within a range from (0 - SLOPE_RANGE_SIZE), offset by SLOPE_RANGE_OFFSET
        this.slope = rng.nextInt(SLOPE_RANGE_SIZE + 1) + SLOPE_RANGE_OFFSET;
        //random y-intercept within a range from (0 - Y_INT_RANGE_SIZE), offset by Y_INT_RANGE_OFFSET

        this.yIntercept = rng.nextInt(Y_INT_RANGE_SIZE + 1) + Y_INT_RANGE_OFFSET; //random y-intercept between -50 and 50
        this.answer = calculateRandomAnswer(10, -10);   //by default, the answer is a random x value on the line.
        createMathematicalQuestion();
    }

    //constructor, with custom range size of possible x values and a lowerBound value (lowest x value in range)
    public LinearEquationGenerator(String questionGoal, Random rng, int xAnswerRangeSize, int xAnswerLowerBound) {
        super(questionGoal, rng);

        this.slope = rng.nextInt(SLOPE_RANGE_SIZE + 1) + SLOPE_RANGE_OFFSET;
        this.yIntercept = rng.nextInt(Y_INT_RANGE_SIZE + 1) + Y_INT_RANGE_OFFSET; 

        this.answer = calculateRandomAnswer(xAnswerRangeSize, xAnswerLowerBound);
        createMathematicalQuestion();
    }

    public int getAnswer() {
        return this.answer;
    }

    //USED ONLY FOR JUnit to test equation string
    public LinearEquationGenerator(String questionGoal, Random rng, int slope, int yIntercept, int answer) {
        super(questionGoal, rng);
        this.slope = slope;
        this.yIntercept = yIntercept;
        this.answer = answer;
    }


    //Create a Question displaying the question statement depending on the goal of the question (solvex, findslope)
    //depending on the goal, the answer field will be set accordingly.
    @Override
    public Question createMathematicalQuestion() {

        if (super.getQuestionGoal().equals("solvex")) {
            Question newQuestion = new SimpleQuestion("Given the linear equation " + linearEquationRule()
            + " find x if the y value of a point of this line is " + calculateYfromX(this.answer) + ". ", ""+this.answer);
            return newQuestion;
        } else {
            setSlopeAsAnswer();
            Question newQuestion = new SimpleQuestion("Given the following x and y values: " + coordinateTable() + "find the slope of this line.", ""+this.answer);
            return newQuestion;
        }
    }

    //calculate y given an x-coordinate given this slope and intercept
    public int calculateYfromX(int xCoordinate) {
        return this.slope*xCoordinate + this.yIntercept;
    }

    //create a linear equation string representation in the form of y = mx + b
    public String linearEquationRule() {

        String linearEquation = "y = ";
        //equation rule must be displayed in a different way for special cases of of y and x values
        if (this.slope == 1) {
            linearEquation += "x ";
        } else if (this.slope == -1) {
            linearEquation += "-x ";
        } else {
            linearEquation += this.slope + "x ";
        }

        //for negative y-intercepts, the absolute value of y-intercept is displayed, with a subtraction operator
        //for y intercepts > 0, display intercept
        //for y intercepts equal 0, leave it as y = mx
        if (this.yIntercept < 0) {
            linearEquation += "- " + Math.abs(this.yIntercept);
        } else if (this.yIntercept > 0) { 
            linearEquation += "+ " + this.yIntercept;
        }
        
        return linearEquation;
        
    }
    //helper methods======================

    //sets a random x answer if the goal for the question is to find x.
    private int calculateRandomAnswer(int rangeSize, int lowerBound) {
        return rng.nextInt(rangeSize + 1) + lowerBound; 
        //random x answer between lowerBound and (lowerBound + rangeSize) = upper value of the range
    }

    //sets the slope as the answer if the goal for the question is to find the slope.
    private void setSlopeAsAnswer() {
        this.answer = this.slope;
    }


    //create a string containing a table of coordinate values of points on this line.
    private String coordinateTable() {
        final int START_X = this.answer - rng.nextInt(21); //random startinf x coordinate offset from answer (why 21?)
        final int X_INCREMENT = rng.nextInt(5) + 1;//random x increment between rows  (why 5?)
        final int NUMBER_ROWS = rng.nextInt(6) + 5;//random number of rows between 5 and 10 (why 6?)
        StringBuilder sb = new StringBuilder();
        sb.append("\n\tx\t|y\t\n");
        sb.append("\t============\n");


        //each iteration adds a row to the table string.
        for (int i = 0; i < NUMBER_ROWS + 1; i++) {
            int pointX = (START_X + (i * X_INCREMENT));
            int pointY = calculateYfromX(pointX);
            sb.append("\t" + pointX + "\t|" + pointY + "\t\n");
        }

        return sb.toString();
    }
}
