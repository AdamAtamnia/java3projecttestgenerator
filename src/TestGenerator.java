package src;
import java.io.IOException;
import java.nio.file.NoSuchFileException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

/**
 * This class is reponsible for generating tests given a file (with strings to parse) by the teacher
 * which will output a generated exam into a file.
 * @author Adam, Kyle
 */
public class TestGenerator {


    private List<Question> questions;
    private List<String> questionStrings;
    public TestGenerator(String inFilePath, String outFilePath) {

        Pattern pattern = Pattern.compile(".txt$");
        if (!pattern.matcher(inFilePath).find() || !pattern.matcher(outFilePath).find()) {
            throw new IllegalArgumentException("Error, all files have to be .txt, try again!");
        }

        FileInOut fileInOut = new FileInOut(inFilePath, outFilePath);
        try {
            this.questionStrings = fileInOut.importFile();
            this.questions = this.generateQuestionList();        

            //Create a large string, appending the question string per question which is then written to the output file
            String builder = questionsToString();
            System.out.println(builder);
            fileInOut.writeStringToFile(builder);

        } catch (NoSuchFileException e) {
            throw new IllegalArgumentException("Generator: file not found, try again!");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private String questionsToString() {
        StringBuilder builder = new StringBuilder();
        for (Question question : this.questions) {
            builder.append(question.makeQuestionString()+"\n\n");
        }
        return builder.toString();
    }

    
    public TestGenerator(Exam exam, String recreateOutFilePath) {

        Pattern pattern = Pattern.compile(".txt$");
        if (!pattern.matcher(recreateOutFilePath).find()) {
            throw new IllegalArgumentException("Error, all files have to be .txt, try again!");
        }

        FileInOut fileInOut = new FileInOut("files\\test.txt", recreateOutFilePath);
        try {     
            //Create a large string, appending the question string per question which is then written to the output file
            System.out.println(examToSting(exam));
            fileInOut.writeStringToFile(examToSting(exam));

        } catch (NoSuchFileException e) {
            throw new IllegalArgumentException("File not found");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private String examToSting(Exam exam) {
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < exam.getNumberQuestions(); i++) {
            builder.append(exam.getQuestion(i).makeQuestionString()+"\n\n");
        }
        return builder.toString();
    }

    public TestGenerator(List<String> questionStrings) {
        this.questionStrings = questionStrings;
        this.questions = this.generateQuestionList();
    }

    public List<Question> getQuestions() {
        return new ArrayList<Question>(questions);
    }


    private List<Question> generateQuestionList(){
        List<Question> questionList = new ArrayList<Question>();
        for (String string : this.questionStrings) {
            if (!string.equals("")) {
                String[] strings = string.split("\\|");
                final int QUESTION_TYPE_COLUMN = 0;

                //determine which parser to use to create the desired type of Question
                switch (strings[QUESTION_TYPE_COLUMN].toLowerCase()) {
                    case "simplequestion":

                        //format:   simplequestion|<correctAnswer>
                        questionList.add(QuestionAnswerParsers.parseSimpleQuestion(string));
                        break;
                    case "multiplechoicequestion":

                        //format:   multiplechoicequestion|<question text>|<answerA>,<answerB>..|<correctAnswer>
                        questionList.add(QuestionAnswerParsers.parseMultipleChoice(string)); 

                        break;
                    case "fillblankquestion":

                        //format:   fillblankquestion|<question text (must contain correctAnswer)>||<correctAnswer>
                        questionList.add(QuestionAnswerParsers.parseFillBlank(string));
                        break;
                    case "math-linear" :

                        //users can input in 2 formats
                        //format: math-linear|<goal> which sets a default range of possible x answers
                        //OR format: math-linear|<goal>|<xrangesize>|<xrangelowerbound> to set a custom range of possible x answers
                        determineLinearParser(questionList, string);
                        break;
                    case "math-parabola" :
                
                        //users can input in 1 format only; no customization
                        //format: math-linear|<goal>
                        questionList.add(QuestionAnswerParsers.parseMathParabolaQuestion(string));
                        break;
                    default:
                        throw new IllegalArgumentException("At least one question type string is invalid in your inputed file");
                }
            }   
        }
        return questionList;
    }

    //for math linear questions, determine parser to use depending on given format
    private void determineLinearParser(List<Question> questionList, String string) {
        if (string.split("\\|").length == 2) {

            questionList.add(QuestionAnswerParsers.parseMathLinearQuestion(string));
        } else if (string.split("\\|").length == 4) {
  
            questionList.add(QuestionAnswerParsers.parseMathLinearQuestionCustomRange(string));
        }
    }

}