package src;


/**
 * This class is for creating Simple questions (Question and answer). 
 * The answer must be exactly written in order to be correct.
 * @author Adam
 */
public class SimpleQuestion extends Question {

    public SimpleQuestion(String question, String answer){
        super(question, answer, null);
    }

    public SimpleQuestion(Question question, String studentAnswer){
        super(question.getQuestion(), question.getAnswer(), studentAnswer);
    }

    @Override //testing purposes only
    public String makeQuestionString() {
        return super.getQuestion() + "___________________";
    }


    @Override
    public Question addStudentAnswer(Question oldQuestion, String studentAnswer) {
        Question withStudentAnswer = new SimpleQuestion(oldQuestion, studentAnswer);
        return withStudentAnswer;
    }

}
