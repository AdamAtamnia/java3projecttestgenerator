package src;

import java.util.List;

/**
 * Stores an exam: comprised of a name and a list of questions
 * @author Adam
 */
public class Exam {
    private String name;
    private List<Question> questions;

    public Exam(String name, List<Question> questions){
        this.name = name;
        this.questions = questions;

    }

    public String getName() {
        return name;
    }

    public Question getQuestion(int index) {
        return this.questions.get(index);
    }

    public int getNumberQuestions() {
        return this.questions.size();
    }
}
