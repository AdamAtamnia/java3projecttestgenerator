package src;
import java.util.Random;

/**
 * This class is an abstract class where the inheriting classes will be responsible for generating different types of math problems
 * @author Kyle
 */
public abstract class MathGenerator {


    protected Random rng;
    private String correctAnswer;    //the correct answer, a number which will be in a String format.
    private String questionGoal;     //the goal of the question (ex: if it's a linear question, one goal would be find x given y.)

    public MathGenerator(String questionGoal, Random rng) {
        this.rng = rng;
        this.questionGoal = questionGoal;
    }


    //create a methematical question method varies between different math questions
    public abstract Question createMathematicalQuestion();


    public String getCorrectAnswer() {
        return this.correctAnswer;
    }

    public String getQuestionGoal() {
        return this.questionGoal;
    }

}
