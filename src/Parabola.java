package src;

import java.util.Random;


/**
 * This class is responsible for generating parabola equations
 * defined by its equation in vertex form: y = a(x - h)^2 + k.
 * The goal of the question will depend on the input given in the constructor.
 * 
 * ==possible goals==
 * -solvey: solve y given a random x coordinate on this parabola
 * -findparametera (the default): find the a parameter (parabola widening/narrowing modifier) given a table of values
 * @author Kyle
 */
public class Parabola extends MathGenerator {

    private int answer;       //answer will vary depending on the question goal. This will be the answer when it uses the Question class

    private int hVertex;      //the x coordinate of the vertex
    private int kVertex;      //the y coordinate of the vertex

    private int aParameter;   //the parameter that makes the parabola narrow or wider.
    
    
    private static final int MIN_VALUE_A_PARAM = -4; //the minimum value of a parameter
    private static final int A_PARAM_RANGE_SIZE = 8; //how many possible values of the a parameter, inclusive
    private static final int VERTEX_COORDINATE_RANGE_SIZE = 10; //how many possible values can the vertex coordinates be (both x and y)


    private static final int VERTEX_COORDINATE_RANGE_OFFSET = -5; //offset, the min x (and y) value possible; the lowerbound of the range

    public Parabola(String questionGoal, Random rng) {
        super(questionGoal, rng);
        //set the a parameter

        this.aParameter = rng.nextInt(A_PARAM_RANGE_SIZE + 1) + MIN_VALUE_A_PARAM;
        //if the value of a happens to be 0, set it to another random value, and if 0, repeat.
        while (this.aParameter == 0) {
            this.aParameter = rng.nextInt(A_PARAM_RANGE_SIZE + 1) + MIN_VALUE_A_PARAM;
        }

        //set the vertex coordinates
        this.hVertex = rng.nextInt(VERTEX_COORDINATE_RANGE_SIZE + 1) + VERTEX_COORDINATE_RANGE_OFFSET;
        this.kVertex = rng.nextInt(VERTEX_COORDINATE_RANGE_SIZE + 1) + VERTEX_COORDINATE_RANGE_OFFSET;
  
        createMathematicalQuestion();

    }

    //USED ONLY FOR JUnit
    public Parabola(String questionGoal, Random rng, int aParameter, int hVertex, int kVertex) {
        super(questionGoal, rng);
        this.aParameter = aParameter;
        this.hVertex = hVertex;
        this.kVertex = kVertex;
        createMathematicalQuestion();

    }

    public int getAnswer() {
        return this.answer;
    }

    @Override
    public Question createMathematicalQuestion() {
        if (super.getQuestionGoal().equals("solvey")) {
            int chosenXCoordinate = randomXCoordinateOnParabola(); //pick a x value on parabola
            this.answer = calculateYfromX(chosenXCoordinate); //set the answer field to whatever y value would be for this parabola

            Question newQuestion = new SimpleQuestion("Given the equation of a parabola " + parabolaEquationRule()
            + " find y if the x value is " + chosenXCoordinate + ". ", ""+this.answer);
            return newQuestion;
        } else {
            //by default (or if teacher input an invalid quesiton goal, the default goal is "findparametera")
            this.answer = this.aParameter; //set the answer to this parabola's a parameter

            Question newQuestion = new SimpleQuestion("The following parabola passes through the following points: " + coordinateTable() + "find the value of the a parameter in the rule y = a(x - h)^2 + k.", ""+this.answer);
            return newQuestion;
        }
    }

    //create a parabola equation string in vertex form y = a(x - h)^2 + k
    public String parabolaEquationRule() {
        String parabolaEquation = "y = ";
        
        //append the value of a (if it's 1, do not display it). (If it is -1, append only a negative sign). For negative a values, append minus sign, then their absolute value
        parabolaEquation += (this.aParameter < 0 ? "-" : "") + (Math.abs(this.aParameter) == 1 ? "" : Math.abs(this.aParameter));
        
        //if x coordinate of vertex is 0 (hVertex), do not display it in rule
        //if positive, value is displayed, with a subtraction operator
        //if negative their absolute value is displayed, with an addition value
        if (this.hVertex == 0) {
            parabolaEquation += "x^2 ";
        } else if (this.hVertex > 0) {
            parabolaEquation += "(x - " + this.hVertex + ")^2 ";
        } else {
            parabolaEquation += "(x + " + Math.abs(this.hVertex) + ")^2 ";
        }
        //for negative k vertex values, their absolute value is displayed, with a subtraction operator
        //for k vertex > 0, display intercept
        //for k vertexs equal 0, leave it
        if (this.kVertex < 0) {
            parabolaEquation += "- " + Math.abs(this.kVertex);
        } else if (this.kVertex > 0) { 
            parabolaEquation += "+ " + this.kVertex;
        }

        return parabolaEquation;
    }

    

    //calculate y given an x-coordinate of this parabola
    public int calculateYfromX(int xCoordinate) {
        //calculate using the rule y = a(x - h)^2 + k
        return this.aParameter * (int)Math.pow((xCoordinate - this.hVertex), 2) + this.kVertex;
    }

    //helper methods======================

    //choose a random x point on the parabola. picks a random x coordinate
    private int randomXCoordinateOnParabola() {

        final int CHOSEN_X_SIZE = 10; //how many possible x values to be picked
        final int MINIMUM_X = -5;        //the lowest x value
        return rng.nextInt(CHOSEN_X_SIZE + 1) + MINIMUM_X; 
        
    }
    
    
    //create a string containing a table of coordinate values of points of this parabola
    private String coordinateTable() {
        final int LARGEST_X_OFFSET = 5; //largest offset the starting x coordinate can be shifted from the vertex of parabola
        final int MIN_NUMBER_ROWS = 6;  //min number of rows to be on the table (this should be larger than the largest offset value)
        final int MAX_EXTRA_ROWS = 4; //maximun extra rows the table can have at random

        final int START_X = this.hVertex - rng.nextInt(LARGEST_X_OFFSET);     //the starting x coordinate
        final int NUMBER_ROWS = rng.nextInt(MAX_EXTRA_ROWS) + MIN_NUMBER_ROWS;//add MIN_NUMBER_ROWS plus a random number of extra rows
        StringBuilder sb = new StringBuilder();
        sb.append("\n\tx\t|y\t\n");
        sb.append("\t============\n");


        //each iteration adds a row to the table string. the point x increment is always 1 per iteration.
        for (int i = 0; i < NUMBER_ROWS + 1; i++) {
            int pointX = (START_X + i);
            int pointY = calculateYfromX(pointX);
            sb.append("\t" + pointX + "\t|" + pointY + "\t\n");
        }

        return sb.toString();
    }

    

}
