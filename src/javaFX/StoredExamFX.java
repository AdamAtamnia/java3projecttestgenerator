package src.javaFX;

import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;


/**
 * Represents one stored exam in the UI
 * @author Adam
 */
public class StoredExamFX extends VBox{

    private TextField recreateOutputPath;
    private Button recreateExamBtn;
    private Text recreateMessage;

    private TextField gradeInputPath;
    private TextField gradeOutputPath;
    private Button gradeExamBtn;
    private Text gradeMessage;

    public StoredExamFX(String name){
        Text examName = new Text(name);
        
        Text recreateSectionTitle = new Text("Recreate Exam:");
        this.recreateOutputPath = new TextField("output.txt");
        this.recreateExamBtn = new Button("Recreate");
        this.recreateMessage = new Text();

        HBox recreateSection = new HBox();
        recreateSection.getChildren().addAll(recreateOutputPath, recreateExamBtn);

        Text gradeSectionTitle = new Text("Grade Exam:");
        this.gradeInputPath = new TextField("input.txt");
        this.gradeOutputPath = new TextField("output.txt");
        this.gradeExamBtn = new Button("Grade");
        this.gradeMessage = new Text();
        
        HBox gradeSection = new HBox();
        gradeSection.getChildren().addAll(gradeInputPath, gradeOutputPath, gradeExamBtn);

        this.getChildren().addAll(examName, recreateSectionTitle, recreateSection, recreateMessage, gradeSectionTitle, gradeSection, gradeMessage);
    }

    public TextField getRecreateOutputPath() {
        return recreateOutputPath;
    }

    public Button getRecreateExamBtn() {
        return recreateExamBtn;
    }

    public TextField getGradeInputPath() {
        return gradeInputPath;
    }

    public TextField getGradeOutputPath() {
        return gradeOutputPath;
    }

    public Button getGradeExamBtn() {
        return gradeExamBtn;
    }

    public Text getRecreateMessage() {
        return recreateMessage;
    }

    public Text getGradeMessage() {
        return gradeMessage;
    }
}
