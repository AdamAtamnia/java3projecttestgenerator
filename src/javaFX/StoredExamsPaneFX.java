package src.javaFX;

import javafx.scene.layout.VBox;
import javafx.scene.text.Text;


/**
 * Represents the pane storing all the stored exams
 * @author Adam
 */
public class StoredExamsPaneFX extends VBox{
    
    private VBox storedExamsFX;
    private Text count;

    public StoredExamsPaneFX(){
        storedExamsFX = new VBox();
        Text title = new Text("Stored Exams:");
        count = new Text("Count: "+this.getExamCount());
        this.getChildren().addAll(title, count, storedExamsFX);
    }

    public void addStoredExam(StoredExamFX exam){
        this.storedExamsFX.getChildren().add(exam);
        this.count.setText("Count: "+this.getExamCount());
    }

    public int getExamCount(){
        return this.storedExamsFX.getChildren().size();
    }
}
