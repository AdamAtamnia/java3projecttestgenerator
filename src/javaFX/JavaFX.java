package src.javaFX;

import javafx.application.*;
import javafx.stage.*;
import src.AllExams;
import src.eventHandlers.CreateExamHandler;
import javafx.scene.*;
import javafx.scene.paint.*;
import javafx.scene.layout.*;

/**
 * main UI application
 * @author Adam
 */
public class JavaFX extends Application {

    @Override
    public void start(Stage stage) {
        // container and layout
        Group root = new Group();

        // scene is associated with container, dimensions
        Scene scene = new Scene(root, 400, 300);
        scene.setFill(Color.WHITE);

        CreateExamFX mainPane = new CreateExamFX();
        StoredExamsPaneFX sideBar = new StoredExamsPaneFX();

        HBox hb = new HBox();
        hb.getChildren().addAll(mainPane, sideBar);
        root.getChildren().addAll(hb);
        AllExams allExams = new AllExams();

        mainPane.getCreateExamBtn().setOnAction(new CreateExamHandler(mainPane, sideBar, allExams));

        stage.setTitle("Text Generator and Grader");
        stage.setScene(scene);
        stage.show();
    }

    public static void main(String[] args) {
        Application.launch(args);
    }
}
