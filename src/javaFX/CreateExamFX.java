package src.javaFX;

import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;

public class CreateExamFX extends VBox{

    private Text title;
    private TextField examName;
    private TextField inputPath;
    private TextField outputPath;
    private Button createExamBtn;

    private Text message;


    /**
     * Represents the main pane in the UI used to create an exam.
     * @author Adam
     */
    public CreateExamFX(){
        // make text fields
        //create text Field inputField
        //this.getChildren().add(inputField);
        //check slides for how to connect 2 panes with event listeners

        this.title = new Text("Create Exam:");

        this.examName = new TextField("Exam Name");
        this.inputPath = new TextField("input.txt");
        this.outputPath = new TextField("output.txt");

        this.createExamBtn = new Button("Create");

        this.message = new Text();

        this.getChildren().addAll(title, examName, inputPath, outputPath, createExamBtn, message);
    }

    public Text getMessage() {
        return message;
    }

    public Button getCreateExamBtn() {
        return createExamBtn;
    }

    public TextField getExamName() {
        return examName;
    }

    public TextField getInputPath() {
        return inputPath;
    }

    public TextField getOutputPath() {
        return outputPath;
    }

}
