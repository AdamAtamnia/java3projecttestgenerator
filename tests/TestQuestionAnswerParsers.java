package tests;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.Test;

import src.FillBlankQuestion;
import src.MultipleChoiceQuestion;
import src.QuestionAnswerParsers;
import src.SimpleQuestion;
import java.util.ArrayList;
import java.util.List;

public class TestQuestionAnswerParsers {
    

    //Unit testing Parsers in charge of making multiple choice

    @Test
    public void testParseMultipleChoice() {
        String stringFormat = "multiplechoicequestion|Question?|an answer,another answer,unknown answer,123|123";
        List<String> expectedAnswers = new ArrayList<String>();
        expectedAnswers.add("an answer");
        expectedAnswers.add("another answer");
        expectedAnswers.add("unknown answer");
        expectedAnswers.add("123");
        MultipleChoiceQuestion expectedQuestion = new MultipleChoiceQuestion("Question?", "123", expectedAnswers);
        MultipleChoiceQuestion actualQuestion = (MultipleChoiceQuestion)QuestionAnswerParsers.parseMultipleChoice(stringFormat);
        
        //check if they are equal MultipleChoiceQuestion objects
        assertEquals(true, expectedQuestion.equals(actualQuestion));
    }


    //test a given string format with an invalid parsed possible size of answers (1 choice of answers)
    @Test
    public void testParseMultipleChoiceFormattingError() {
        String stringFormat = "multiplechoicequestion|AnswerA,AnswerB,AnswerC|AnswerB|Question text here|AnswerB";
        
    
        try {
            QuestionAnswerParsers.parseMultipleChoice(stringFormat);
            fail("This should have thrown an exception");
        } catch (IllegalArgumentException e) {
            //this passes
        } catch (Exception e) {
            fail("Other type of exception caught.");
        }

    }


    //test a given string format with an invalid parsed possible size of answers (1 choice of answers)
    @Test
    public void testParseMultipleChoiceInvalidChoiceSize() {
        String stringFormat = "multiplechoicequestion|Question?|ChoiceA|ChoiceA";
        
    
        try {
            QuestionAnswerParsers.parseMultipleChoice(stringFormat);
            fail("This should have thrown an exception");
        } catch (IllegalArgumentException e) {
            //this passes
        } catch (Exception e) {
            fail("Other type of exception caught.");
        }

    }
    //test a given string format with an invalid parsed possible size of answers (6 choices of answers)
    @Test
    public void testParseMultipleChoiceInvalidChoiceSize2() {
        String stringFormat = "multiplechoicequestion|Question?|ChoiceA,ChoiceB,ChoiceC,ChoiceD,ChoiceE,ChoiceF|ChoiceA";
        
    
        try {
            QuestionAnswerParsers.parseMultipleChoice(stringFormat);
            fail("This should have thrown an exception");
        } catch (IllegalArgumentException e) {
            //this passes
        } catch (Exception e) {
            fail("Other type of exception caught.");
        }

    }
    //test a given string format where the correct answer isnt one of the possible answer choices
    @Test
    public void testParseMultipleChoiceInvalidDoesntContainAnswer() {
        String stringFormat = "multiplechoicequestion|Which is a primary Colour|Indigo,Red,Green|New York";
        
        try {
            QuestionAnswerParsers.parseMultipleChoice(stringFormat);
            fail("This should have thrown an exception");
        } catch (IllegalArgumentException e) {
            //this passes.
        } catch (Exception e) {
            fail("Other type of exception caught.");
        }

    }

    @Test
    public void testParseSimpleQuestion() {
        String stringFormat = "simplequestion|Is this a unit test class?|yes";
        
        SimpleQuestion expectedQuestion = new SimpleQuestion("Is this a unit test class?", "yes");
        SimpleQuestion actualQuestion = (SimpleQuestion)QuestionAnswerParsers.parseSimpleQuestion(stringFormat);

        //check if they are equal SimpleQuestion objects
        assertEquals(true, expectedQuestion.equals(actualQuestion));
    }


    //Unit testing Parsers in charge of making fill in blank questions

    @Test
    public void testParseFillBlankQuestion() {
        String stringFormat = "fillblankquestion|This is a fill in the blank question|blank";
        FillBlankQuestion expectedQuestion = new FillBlankQuestion("This is a fill in the blank question", "blank");
        FillBlankQuestion actualQuestion = (FillBlankQuestion)QuestionAnswerParsers.parseFillBlank(stringFormat);

        //check if they are equal FillBlankQuestion objects
        assertEquals(true, expectedQuestion.equals(actualQuestion));

    }

    //test a given string format where the question text does not include the answer (resulting in a question without a blank)
    @Test
    public void testParseFillBlankQuestionInvalidDoesntIncludeAnswer() {
        String stringFormat = "fillblankquestion|Kyle will be single forever.|New York";
        
    
        try {
            QuestionAnswerParsers.parseFillBlank(stringFormat);
            fail("This should have thrown an exception");
        } catch (IllegalArgumentException e) {
            //this passes
        } catch (Exception e) {
            fail("Other type of exception caught.");
        }

    }


    //Unit testing Parsers in charge of making MathLinearQuestions...
    
    @Test
    public void parseMathLinearQuestionFormattingError() {
        String stringFormat = "math-linear|Question text here|findrule";

        try {
            QuestionAnswerParsers.parseMathLinearQuestion(stringFormat);
            fail("This should have thrown an exception");
        } catch (IllegalArgumentException e) {
            //this passes
        } catch (Exception e) {
            fail("Other type of exception caught.");
        }

    }

    @Test
    public void parseMathLinearQuestionCustomRangeFormattingError() {
        String stringFormat = "math-linear|Question text here|solvex|5|0";

        try {
            QuestionAnswerParsers.parseMathLinearQuestionCustomRange(stringFormat);
            fail("This should have thrown an exception");
        } catch (IllegalArgumentException e) {
            //this passes
        } catch (Exception e) {
            fail("Other type of exception caught.");
        }

    }

    @Test
    public void parseMathLinearQuestionInvalidXRangeSize() {
        String stringFormat = "math-linear|solvex|-50|-10";

        try {
            QuestionAnswerParsers.parseMathLinearQuestionCustomRange(stringFormat);
            fail("This should have thrown an exception");
        } catch (IllegalArgumentException e) {
            //this passes
        } catch (Exception e) {
            fail("Other type of exception caught.");
        }

    }

    //Unit testing Parsers in charge of making Parabola Questions...
    @Test
    public void parseMathParabolaQuestionFormattingError() {
        String stringFormat = "math-parabola|Question text here|findrule";

        try {
            QuestionAnswerParsers.parseMathParabolaQuestion(stringFormat);
            fail("This should have thrown an exception");
        } catch (IllegalArgumentException e) {
            //this passes
        } catch (Exception e) {
            fail("Other type of exception caught.");
        }

    }

    //even though an invalid questionGoal string is given, it still should not throw exception as it will
    // provide a parabola question with goal to find parameter a as the default
    @Test
    public void parseMathParabolaQuestionDefaultGoal() {
        String stringFormat = "math-parabola|othergoal";
        try {
            QuestionAnswerParsers.parseMathParabolaQuestion(stringFormat);
        } catch (IllegalArgumentException e) {
            fail("This should not happen, this should have NOT thrown an IllegalArgumentexception");
        } catch (Exception e) {
            fail("This should not happen, other problem with this.");
        }

    }


}
