package tests;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.Test;

import src.MultipleChoiceQuestion;
import src.Question;

import java.util.ArrayList;
import java.util.List;

public class TestMultipleChoice {

    @Test
    public void testGetPossibleAnswers() {
        List<String> posAns = new ArrayList<String>();
        posAns.add("Montreal");
        posAns.add("Trois-Rivieres");
        posAns.add("Ottawa");
        posAns.add("Edmonton");
        MultipleChoiceQuestion testQuestion = new MultipleChoiceQuestion("Which of these is a good city:", "Ottawa", posAns);
        assertEquals(posAns, testQuestion.getPossibleAnswers());
    }

    @Test
    public void testGetQuestion() {
        List<String> posAns = new ArrayList<String>();
        posAns.add("Montreal");
        posAns.add("Trois-Rivieres");
        posAns.add("Ottawa");
        posAns.add("Edmonton");
        Question testQuestion = new MultipleChoiceQuestion("Which of these is a good city:", "Ottawa", posAns);
        assertEquals("Which of these is a good city:", testQuestion.getQuestion());
    }

    @Test
    public void testGetQuestionTwoPossibleAnswers() {
        List<String> posAns = new ArrayList<String>();
        posAns.add("True");
        posAns.add("False");
        Question testQuestion = new MultipleChoiceQuestion("The sky is red.", "False", posAns);
        assertEquals("The sky is red.", testQuestion.getQuestion());
    }

    @Test
    public void testMakeQuestionString() {
        List<String> posAns = new ArrayList<String>();
        posAns.add("January");
        posAns.add("Jett");
        posAns.add("September");
        posAns.add("July");
        Question testQuestion = new MultipleChoiceQuestion("Which of these is not like the others?", "Jett", posAns);
        String validQuestionString = "Which of these is not like the others?"
                                      + "\nA) January\nB) Jett\nC) September\nD) July";
        assertEquals(validQuestionString, testQuestion.makeQuestionString());
    }


}
