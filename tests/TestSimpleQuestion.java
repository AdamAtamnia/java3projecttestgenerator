package tests;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.Test;

import src.Question;
import src.SimpleQuestion;

public class TestSimpleQuestion {
    @Test
    public void testMakeQuestionString(){
        Question question = new SimpleQuestion("What is my name?","Bob");
        assertEquals("What is my name?___________________", question.makeQuestionString());
    }
}
