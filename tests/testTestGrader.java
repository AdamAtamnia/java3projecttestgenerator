package tests;
import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.List;
import org.junit.Test;
import src.Exam;
import src.FillBlankQuestion;
import src.MultipleChoiceQuestion;
import src.Question;
import src.SimpleQuestion;
import src.TestGrader;

public class testTestGrader {

    
    /*
    Notes: In test cases for correcting questions, a TestGrader object is created, taking input an arrayList containing one answer, the student's answer.
    */

    //testing a 2-question exam, two incorrect answers.
    @Test
    public void testNoneCorrectTwoTotal() {
        List<Question> sampleQuestions = new ArrayList<Question>();
        List<String> sampleStudentAnswers = new ArrayList<String>();
        sampleQuestions.add(new SimpleQuestion("Some people have blue eyes", "true"));
        sampleQuestions.add(new FillBlankQuestion("Speaking of seasons, January is one of the winter months.", "winter"));
        sampleStudentAnswers.add("false");
        sampleStudentAnswers.add("coldest");

        Exam sampleExam = new Exam("name", sampleQuestions);
        //sample testGrader
        TestGrader testGrader = new TestGrader(sampleStudentAnswers);
        testGrader.correctTest(sampleExam, sampleStudentAnswers);
        assertEquals(0, testGrader.getNumberCorrect());
    }

    //testing a 3-question exam, 1 correct answer, 2 incorrect
    @Test
    public void testOneCorrectThreeTotal() {
        List<Question> sampleQuestions = new ArrayList<Question>();
        List<String> sampleStudentAnswers = new ArrayList<String>();
        List<String> possibleAnswers = new ArrayList<String>();
        possibleAnswers.add("red");
        possibleAnswers.add("green");
        possibleAnswers.add("blue");
        possibleAnswers.add("yellow");

        sampleQuestions.add(new SimpleQuestion("Some people have blue eyes", "true"));
        sampleQuestions.add(new FillBlankQuestion("Speaking of seasons, January is one of the winter months.", "winter"));
        sampleQuestions.add(new MultipleChoiceQuestion("Which of these is not a primary colour", "yellow", possibleAnswers));
        sampleStudentAnswers.add("false");
        sampleStudentAnswers.add("coldest");
        sampleStudentAnswers.add("yellow");

        Exam sampleExam = new Exam("name", sampleQuestions);
        //sample testGrader
        TestGrader testGrader = new TestGrader(sampleStudentAnswers);
        testGrader.correctTest(sampleExam, sampleStudentAnswers);
        assertEquals(1, testGrader.getNumberCorrect());
        assertEquals(3, testGrader.getNumberAnswered());
    }

    //testing a 3-question exam, 3 correct (all correct)
    @Test
    public void testAllCorrectThreeTotal() {
        List<Question> sampleQuestions = new ArrayList<Question>();
        List<String> sampleStudentAnswers = new ArrayList<String>();
        List<String> possibleAnswers = new ArrayList<String>();
        possibleAnswers.add("red");
        possibleAnswers.add("green");
        possibleAnswers.add("blue");
        possibleAnswers.add("yellow");

        sampleQuestions.add(new SimpleQuestion("Some people have blue eyes", "true"));
        sampleQuestions.add(new FillBlankQuestion("Speaking of seasons, January is one of the winter months.", "winter"));
        sampleQuestions.add(new MultipleChoiceQuestion("Which of these is not a primary colour", "yellow", possibleAnswers));
        sampleStudentAnswers.add("true");
        sampleStudentAnswers.add("winter");
        sampleStudentAnswers.add("yellow");

        Exam sampleExam = new Exam("name", sampleQuestions);
        //sample testGrader
        TestGrader testGrader = new TestGrader(sampleStudentAnswers);
        testGrader.correctTest(sampleExam, sampleStudentAnswers);
        assertEquals(3, testGrader.getNumberCorrect());
        assertEquals(3, testGrader.getNumberAnswered());
    }

    //testing a 3-question exam, 3 correct (all correct), however the case where student provided 5 answers in the List<String>
    @Test
    public void testAllCorrectThreeTotalGivenMoreAnswers() {
        List<Question> sampleQuestions = new ArrayList<Question>();
        List<String> sampleStudentAnswers = new ArrayList<String>();
        List<String> possibleAnswers = new ArrayList<String>();
        possibleAnswers.add("red");
        possibleAnswers.add("green");
        possibleAnswers.add("blue");
        possibleAnswers.add("yellow");

        sampleQuestions.add(new SimpleQuestion("Some people have blue eyes", "true"));
        sampleQuestions.add(new FillBlankQuestion("Speaking of seasons, January is one of the winter months.", "winter"));
        sampleQuestions.add(new MultipleChoiceQuestion("Which of these is not a primary colour", "yellow", possibleAnswers));
        sampleStudentAnswers.add("true");
        sampleStudentAnswers.add("winter");
        sampleStudentAnswers.add("yellow");
        //extra answers student given.
        sampleStudentAnswers.add("another random answer");
        sampleStudentAnswers.add("Merry Christmas!");

        Exam sampleExam = new Exam("name", sampleQuestions);
        //sample testGrader
        TestGrader testGrader = new TestGrader(sampleStudentAnswers);
        testGrader.correctTest(sampleExam, sampleStudentAnswers);
        assertEquals(3, testGrader.getNumberCorrect());
        assertEquals(3, testGrader.getNumberAnswered());
    }

    //testing a 1-question exam, but no answers given at all
    @Test
    public void testThreeTotalNoAnswersGiven() {
        List<Question> sampleQuestions = new ArrayList<Question>();
        List<String> sampleStudentAnswers = new ArrayList<String>(); //EMPTY LIST (NO ANSWERS GIVEN)

        sampleQuestions.add(new SimpleQuestion("Some people have blue eyes", "true"));

        Exam sampleExam = new Exam("name", sampleQuestions);
        TestGrader testGrader = new TestGrader(sampleStudentAnswers);
        testGrader.correctTest(sampleExam, sampleStudentAnswers);
        
        assertEquals(0, testGrader.getNumberCorrect());
        assertEquals(0, testGrader.getNumberAnswered());
    }

    //correcting multiple choice questions
    @Test
    public void testMultipleChoiceRightAnswer() {

        
        List<String> oneAnswer = new ArrayList<String>();
        oneAnswer.add("yellow");
        //sample testGrader
        TestGrader testGrader = new TestGrader(oneAnswer);
        List<String> sampleAnswers = new ArrayList<String>();
        sampleAnswers.add("red");
        sampleAnswers.add("green");
        sampleAnswers.add("blue");
        sampleAnswers.add("yellow");
        
        Question sampleQuestion = new MultipleChoiceQuestion("Which of these is not a primary colour", "yellow", sampleAnswers);

        assertEquals(true, testGrader.correctQuestion(sampleQuestion, oneAnswer.get(0)));
        
    }

    @Test
    public void testMultipleChoiceWrongAnswer() {

        //one question, one answer
        
        //this contains a list with one answer; for testing
        List<String> oneAnswer = new ArrayList<String>();
        oneAnswer.add("true");
        //sample testGrader

        TestGrader testGrader = new TestGrader(oneAnswer);
        List<String> sampleAnswers = new ArrayList<String>();
        sampleAnswers.add("true");
        sampleAnswers.add("false");
    
        Question sampleQuestion = new MultipleChoiceQuestion("The sun is blue.", "false", sampleAnswers);

        assertEquals(false, testGrader.correctQuestion(sampleQuestion, oneAnswer.get(0)));
        
    }

    //testing correcting simple questions
    @Test
    public void testSimpleQuestionRightAnswer() {

        List<String> oneAnswer = new ArrayList<String>();
        oneAnswer.add("hydrogen");
        TestGrader testGrader = new TestGrader(oneAnswer);
        Question sampleQuestion = new SimpleQuestion("What's the first element on the periodic table?", "hydrogen");

        assertEquals(true, testGrader.correctQuestion(sampleQuestion, oneAnswer.get(0)));
        
    }

    @Test
    public void testSimpleQuestionWrongAnswer() {

        List<String> oneAnswer = new ArrayList<String>();
        oneAnswer.add("helium");
        TestGrader testGrader = new TestGrader(oneAnswer);
        Question sampleQuestion = new SimpleQuestion("What's the first element on the periodic table?", "hydrogen");

        assertEquals(false, testGrader.correctQuestion(sampleQuestion, oneAnswer.get(0)));
        
    }

    //testing correcting fill blank questions
    @Test
    public void testFillBlankQuestionRightAnswer() {
        List<String> oneAnswer = new ArrayList<String>();
        oneAnswer.add("worst");
        TestGrader testGrader = new TestGrader(oneAnswer);
        Question sampleQuestion = new FillBlankQuestion("Tuesdays are the best days of the week", "worst");
        System.out.println(sampleQuestion.makeQuestionString());

        assertEquals(true, testGrader.correctQuestion(sampleQuestion, oneAnswer.get(0)));
    }

    @Test
    public void testFillBlankQuestionWrongAnswer() {
        List<String> oneAnswer = new ArrayList<String>();
        oneAnswer.add("annoying");
        TestGrader testGrader = new TestGrader(oneAnswer);
        Question sampleQuestion = new FillBlankQuestion("Tuesdays are the best days of the week", "worst");
        System.out.println(sampleQuestion.makeQuestionString());

        assertEquals(false, testGrader.correctQuestion(sampleQuestion, oneAnswer.get(0)));
    }
}
