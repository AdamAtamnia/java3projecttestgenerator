package tests;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.Test;

import src.FillBlankQuestion;
import src.Question;

public class TestFillBlankQuestion {

    //this should be in testSimpleQuestion
    @Test
    public void testMakeQuestionStringOneWordOccurence(){
        Question question = new FillBlankQuestion("The sun is a star.","star");
        assertEquals("Fill in the blank:\nThe sun is a ______.", question.makeQuestionString());
    }

    //Test cases where the same answer word occurs more than once. Only blanks the first occurance.
    @Test
    public void testMakeQuestionStringMultipleOccurences() {
        Question question = new FillBlankQuestion("Carrots are orange vegetables but not green vegetables.","vegetables");
        assertEquals("Fill in the blank:\nCarrots are orange ______ but not green vegetables.", question.makeQuestionString());
    }

    //Test case where it only blanks out whole words.
    @Test
    public void testMakeQuestionStringBlankFullWord() {
        Question question = new FillBlankQuestion("Jacob has figured out today that New year's day will fall on a Saturday next year.","day");
        assertEquals("Fill in the blank:\nJacob has figured out today that New year's ______ will fall on a Saturday next year.", question.makeQuestionString());
    }

    @Test
    public void testtestMakeQuestionStringWordBoundary() {
        Question question = new FillBlankQuestion("Flourine, .chlorine, and bromine are the first three Halogens.","chlorine");
        assertEquals("Fill in the blank:\nFlourine, .______, and bromine are the first three Halogens.", question.makeQuestionString());
    }
}