package tests;
import static org.junit.jupiter.api.Assertions.*;

import java.util.Random;
import org.junit.Test;

import src.LinearEquationGenerator;
import src.MathGenerator;
import src.LinearEquationGenerator;


/**
 * JUNIT testing for LinearEquationGenerator
 * NOTE(S): Random instances are created, but only used for passing them into the constructor for LinearEquationGenerator
 *  and some tests with the seed (extends MathGenerator abstract classes)
 * @author Kyle
 */
public class TestLinear {


    //Testing if the answer is set accordingly by the questiongoal: solve x
    @Test
    public void testAnswerSolveX(){
        Random rng = new Random(69); 
        MathGenerator sampleLinear = new LinearEquationGenerator("solvex", rng, 5, 3);
        assertEquals(6, ((LinearEquationGenerator)sampleLinear).getAnswer());
    }

    //Testing CalculateYFromX method to ensure the method works properly
    //TEST CASE #1: simple line, m is 1
    @Test
    public void testCalculateYFromX1(){
        Random rng = new Random(); 
        MathGenerator sampleLinear = new LinearEquationGenerator("solvex", rng, 1, 0, 0);
        assertEquals(-2, ((LinearEquationGenerator)sampleLinear).calculateYfromX(-2));
    }

    //TEST CASE #2: m = -6, b = 7
    @Test
    public void testCalculateYFromX2(){
        Random rng = new Random(); 
        MathGenerator sampleLinear = new LinearEquationGenerator("solvex", rng, -6, 7, 0);
        assertEquals(-11, ((LinearEquationGenerator)sampleLinear).calculateYfromX(3));
    }


    //Testing possible cases m and b values of the line
    //since the strings of the linear equation will be different
    //Recall: the equation of a line is defined by y = mx + b

    //TEST CASE #1: m = 1, b = 0 (basic line)
    @Test
    public void testEquationString1(){
        Random rng = new Random(); 
        MathGenerator sampleLinear = new LinearEquationGenerator("solvex", rng, 1, 0, 0);
        assertEquals("y = x ", ((LinearEquationGenerator)sampleLinear).linearEquationRule());
    }

    //TEST CASE #2: m = 3, b = 0 (y intercept 0, positive slope)
    @Test
    public void testEquationString2(){
        Random rng = new Random(); 
        MathGenerator sampleLinear = new LinearEquationGenerator("solvex", rng, 3, 0, 0);
        assertEquals("y = 3x ", ((LinearEquationGenerator)sampleLinear).linearEquationRule());
    }

    //TEST CASE #3: m = -4, b = 0 (y intercept 0, negative slope)
    @Test
    public void testEquationString3(){
        Random rng = new Random(); 
        MathGenerator sampleLinear = new LinearEquationGenerator("solvex", rng, -4, 0, 0);
        assertEquals("y = -4x ", ((LinearEquationGenerator)sampleLinear).linearEquationRule());
    }

    //TEST CASE #4: m = -1, b = 0 (y intercept 0, negative slope #2, coefficient not displayed)
    @Test
    public void testEquationString4(){
        Random rng = new Random(); 
        MathGenerator sampleLinear = new LinearEquationGenerator("solvex", rng, -1, 0, 0);
        assertEquals("y = -x ", ((LinearEquationGenerator)sampleLinear).linearEquationRule());
    }

    //TEST CASE #5: m = 1, b = 10 (y intercept 10, positive 1 slope, y intercept at positve y axis)
    @Test
    public void testEquationString5(){
        Random rng = new Random(); 
        MathGenerator sampleLinear = new LinearEquationGenerator("solvex", rng, 1, 10, 0);
        assertEquals("y = x + 10", ((LinearEquationGenerator)sampleLinear).linearEquationRule());
    }

    //TEST CASE #6: m = -1, b = -13 (y intercept -13, negative 1 slope, y intercept at negative y axis)
    @Test
    public void testEquationString6(){
        Random rng = new Random(); 
        MathGenerator sampleLinear = new LinearEquationGenerator("solvex", rng, -1, -13, 0);
        assertEquals("y = -x - 13", ((LinearEquationGenerator)sampleLinear).linearEquationRule());
    }

    //TEST CASE #7: m = 6, b = 8
    @Test
    public void testEquationString7(){
        Random rng = new Random(); 
        MathGenerator sampleLinear = new LinearEquationGenerator("solvex", rng, 6, 8, 0);
        assertEquals("y = 6x + 8", ((LinearEquationGenerator)sampleLinear).linearEquationRule());
    }

    //TEST CASE #8: m = -2, b = 4
    @Test
    public void testEquationString8(){
        Random rng = new Random(); 
        MathGenerator sampleLinear = new LinearEquationGenerator("solvex", rng, -2, 4, 0);
        assertEquals("y = -2x + 4", ((LinearEquationGenerator)sampleLinear).linearEquationRule());
    }

    //TEST CASE #9: m = -10, b = -10
    @Test
    public void testEquationString9(){
        Random rng = new Random(); 
        MathGenerator sampleLinear = new LinearEquationGenerator("solvex", rng, -10, -10, 0);
        assertEquals("y = -10x - 10", ((LinearEquationGenerator)sampleLinear).linearEquationRule());
    }

    //TEST CASE #10: m = -1, b = 100
    @Test
    public void testEquationString10(){
        Random rng = new Random(); 
        MathGenerator sampleLinear = new LinearEquationGenerator("solvex", rng, -1, 100, 0);
        assertEquals("y = -x + 100", ((LinearEquationGenerator)sampleLinear).linearEquationRule());
    }



}
