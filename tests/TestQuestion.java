package tests;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.Test;

import src.Question;
import src.SimpleQuestion;

public class TestQuestion {
    @Test
    public void getQuestionTest(){
        Question question = new SimpleQuestion("What is my name?","Bob");
        assertEquals("What is my name?", question.getQuestion());
    }
    @Test
    public void getAnswerTest(){
        Question question = new SimpleQuestion("What is my name?","Bob");
        assertEquals("Bob", question.getAnswer());
    }

    @Test
    public void getStudentAnswerTest(){
        Question question = new SimpleQuestion("What is my name?","Bob");
        assertEquals(null, question.getStudentAnswer());
    }

    @Test
    public void setStudentAnswerTest(){
        Question question = new SimpleQuestion("What is my name?","Bob");
        Question question2 = new SimpleQuestion(question,"Yoyo");
        assertEquals("Yoyo", question2.getStudentAnswer());
    }

    @Test
    public void toStringTest(){
        Question question = new SimpleQuestion("What is my name?","Bob");
        Question question2 = new SimpleQuestion(question,"Yoyo");
        //question.setStudentAnswer("Yoyo");

        //1. list1 of questions
        //2. read file answer list2 <= question2 = new SimpleQuestion(question,"Yoyo");
        //3. list2.foreach(question => question.isRightAnswer)
        //4. output to file => grade/list2.size

        assertEquals("Question: What is my name?, Answer: Bob, Student Answer: Yoyo", question2.toString());
    }

    @Test
    public void testException1(){
        try {
            new SimpleQuestion("","Bob");
            fail("Did not catch an IllegalArgumentException");
        } catch (IllegalArgumentException e) {
            //good
        } catch (Exception e){
            fail("Caught unexpected exception");
        }
    }
    @Test
    public void testException2(){
        try {
            new SimpleQuestion("","Bob");
            fail("Did not catch an IllegalArgumentException");
        } catch (IllegalArgumentException e) {
            //good
        } catch (Exception e){
            fail("Caught unexpected exception");
        }
    }

    @Test
    public void testException3(){
        try {
            new SimpleQuestion("What is my name?","");
            fail("Did not catch an IllegalArgumentException");
        } catch (IllegalArgumentException e) {
            //good
        } catch (Exception e){
            fail("Caught unexpected exception");
        }
    }

    @Test
    public void testException4(){
        try {
            new SimpleQuestion("What is my name?",null);
            fail("Did not catch an IllegalArgumentException");
        } catch (IllegalArgumentException e) {
            //good
        } catch (Exception e){
            fail("Caught unexpected exception");
        }
    }
}
