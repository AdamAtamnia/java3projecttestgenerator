package tests;
import static org.junit.jupiter.api.Assertions.*;

import java.util.Random;
import org.junit.Test;
import src.MathGenerator;
import src.Parabola;


/**
 * JUNIT testing for Parabola
 * NOTE(S): Random instances are created, but only used for passing them into the constructor for Parabola
 *  and some tests with the seed (extends MathGenerator abstract classes)
 * @author Kyle
 */
public class TestParabola {


    //Testing if the answer is set accordingly by the questiongoal: solve y
    @Test
    public void testParabolaVertex(){
        Random rng = new Random(69); 
        MathGenerator sampleParabola = new Parabola("solvey", rng, -5, 3, 3);
        assertEquals(-42, ((Parabola)sampleParabola).getAnswer());
    }


    //Testing if the answer is set accordingly by the questiongoal
    @Test
    public void testAnswerFindParameterAGoal(){
        Random rng = new Random(); 
        MathGenerator sampleParabola = new Parabola("findparametera", rng, -5, 0, 0);
        assertEquals(-5, ((Parabola)sampleParabola).getAnswer());
    }

    //if the question goal input string is neither "solvey" or "findparametera" the goal's default is finding parameter a
    @Test
    public void testAnswerFindParameterAGoalDefault(){
        Random rng = new Random(); 
        MathGenerator sampleParabola = new Parabola("invalidquestiongoalinput", rng, 3, 0, 0);
        assertEquals(3, ((Parabola)sampleParabola).getAnswer());
    }


    //Testing CalculateYFromX method to ensure the method works properly

    //TEST CASE #1: simple parabola, y when value of x is at 5
    @Test
    public void testCalculateYFromX1(){
        Random rng = new Random(); 
        MathGenerator sampleParabola = new Parabola("solvey", rng, 1, 0, 0);
        assertEquals(25, ((Parabola)sampleParabola).calculateYfromX(5));
    }

    //TEST CASE #2: parabola with non-zero a, h, k values, y when value of x is at -3
    @Test
    public void testCalculateYFromX2(){
        Random rng = new Random(); 
        MathGenerator sampleParabola = new Parabola("solvey", rng, -1, 2, -7);
        assertEquals(-32, ((Parabola)sampleParabola).calculateYfromX(-3));
    }

    //Testing possible cases of a, h, and k values of the parabola
    //since the strings of the parabola equation will be different
    //Recall: parabolas follow the equation y = a(x - h)^2 + k

    //TEST CASE #1: a = 1, h = 0, k = 0 (basic parabola)
    @Test
    public void testEquationString1(){
        Random rng = new Random(); 
        MathGenerator sampleParabola = new Parabola("solvey", rng, 1, 0, 0);
        assertEquals("y = x^2 ", ((Parabola)sampleParabola).parabolaEquationRule());
    }

    //TEST CASE #2: a = 2, h = 0, k = 0 (vertex at origin, a = 2)
    @Test
    public void testEquationString2(){
        Random rng = new Random(); 
        MathGenerator sampleParabola = new Parabola("solvey", rng, 2, 0, 0);
        assertEquals("y = 2x^2 ", ((Parabola)sampleParabola).parabolaEquationRule());
    }

    //TEST CASE #3: a = -1, h = 0, k = 0 (vertex at origin, a = -1)
    @Test
    public void testEquationString3(){
        Random rng = new Random(); 
        MathGenerator sampleParabola = new Parabola("solvey", rng, -1, 0, 0);
        assertEquals("y = -x^2 ", ((Parabola)sampleParabola).parabolaEquationRule());
    }

    //TEST CASE #4: a = -4, h = 0, k = 0 (vertex at origin, a = -4)
    @Test
    public void testEquationString4(){
        Random rng = new Random(); 
        MathGenerator sampleParabola = new Parabola("solvey", rng, -4, 0, 0);
        assertEquals("y = -4x^2 ", ((Parabola)sampleParabola).parabolaEquationRule());
    }

    //TEST CASE #5: a = 3, h = 5, k = 0 (vertex on positive x axis)
    @Test
    public void testEquationString5(){
        Random rng = new Random(); 
        MathGenerator sampleParabola = new Parabola("solvey", rng, 3, 5, 0);
        assertEquals("y = 3(x - 5)^2 ", ((Parabola)sampleParabola).parabolaEquationRule());
    }

    //TEST CASE #6: a = -3, h = 5, k = 0 (vertex on positive x axis)
    @Test
    public void testEquationString6(){
        Random rng = new Random(); 
        MathGenerator sampleParabola = new Parabola("solvey", rng, -3, 5, 0);
        assertEquals("y = -3(x - 5)^2 ", ((Parabola)sampleParabola).parabolaEquationRule());
    }

    //TEST CASE #7: a = 1, h = 5, k = 0 (vertex on positive x axis)
    @Test
    public void testEquationString7(){
        Random rng = new Random(); 
        MathGenerator sampleParabola = new Parabola("solvey", rng, 1, 5, 0);
        assertEquals("y = (x - 5)^2 ", ((Parabola)sampleParabola).parabolaEquationRule());
    }

    //TEST CASE #8: a = -1, h = 5, k = 0 (vertex on positive x axis)
    @Test
    public void testEquationString8(){
        Random rng = new Random(); 
        MathGenerator sampleParabola = new Parabola("solvey", rng, -1, 5, 0);
        assertEquals("y = -(x - 5)^2 ", ((Parabola)sampleParabola).parabolaEquationRule());
    }

    //TEST CASE #9: a = -1, h = -1, k = 0 (vertex on negative x axis)
    @Test
    public void testEquationString9(){
        Random rng = new Random(); 
        MathGenerator sampleParabola = new Parabola("solvey", rng, -1, -1, 0);
        assertEquals("y = -(x + 1)^2 ", ((Parabola)sampleParabola).parabolaEquationRule());
    }

    //TEST CASE #10: a = 5, h = 3, k = 3 (vertex on quadrant I)
    @Test
    public void testEquationString10(){
        Random rng = new Random(); 
        MathGenerator sampleParabola = new Parabola("solvey", rng, 5, 3, 3);
        assertEquals("y = 5(x - 3)^2 + 3", ((Parabola)sampleParabola).parabolaEquationRule());
    }

    //TEST CASE #11: a = -2, h = -3, k = 3 (vertex on quadrant II)
    @Test
    public void testEquationString11(){
        Random rng = new Random(); 
        MathGenerator sampleParabola = new Parabola("solvey", rng, -2, -3, 3);
        assertEquals("y = -2(x + 3)^2 + 3", ((Parabola)sampleParabola).parabolaEquationRule());
    }

    //TEST CASE #12: a = 1, h = -3, k = -3 (vertex on quadrant III)
    @Test
    public void testEquationString12(){
        Random rng = new Random(); 
        MathGenerator sampleParabola = new Parabola("solvey", rng, 1, -3, -3);
        assertEquals("y = (x + 3)^2 - 3", ((Parabola)sampleParabola).parabolaEquationRule());
    }

    //TEST CASE #13: a = -1, h = 3, k = -3 (vertex on quadrant IV)
    @Test
    public void testEquationString13(){
        Random rng = new Random(); 
        MathGenerator sampleParabola = new Parabola("solvey", rng, -1, 3, -3);
        assertEquals("y = -(x - 3)^2 - 3", ((Parabola)sampleParabola).parabolaEquationRule());
    }

    //TEST CASE #14: a = 4, h = 0, k = 1 (vertex on positive y axis)
    @Test
    public void testEquationString14(){
        Random rng = new Random(); 
        MathGenerator sampleParabola = new Parabola("solvey", rng, 4, 0, 1);
        assertEquals("y = 4x^2 + 1", ((Parabola)sampleParabola).parabolaEquationRule());
    }

    //TEST CASE #15: a = -1, h = 0, k = -5 (vertex on negative y axis)
    @Test
    public void testEquationString15(){
        Random rng = new Random(); 
        MathGenerator sampleParabola = new Parabola("solvey", rng, -1, 0, -5);
        assertEquals("y = -x^2 - 5", ((Parabola)sampleParabola).parabolaEquationRule());
    }
}
