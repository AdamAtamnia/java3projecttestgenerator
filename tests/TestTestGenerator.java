package tests;
import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import src.FillBlankQuestion;
import src.MultipleChoiceQuestion;
import src.Question;
import src.SimpleQuestion;
import src.TestGenerator;

public class TestTestGenerator {
    @Test
    public void testOneQuestion(){

        List<String> questionStrings = new ArrayList<String>();
        questionStrings.add("simplequestion|What do eyes do?|provide vision");
        TestGenerator tg = new TestGenerator(questionStrings);

        List<Question> questions = new ArrayList<Question>();
        Question question = new SimpleQuestion("What do eyes do?","provide vision");
        questions.add(question);

        assertEquals(questions, tg.getQuestions());
    }

    @Test
    public void testManyQuestion(){

        List<String> questionStrings = new ArrayList<String>();
        questionStrings.add("simplequestion|What do eyes do?|provide vision");
        questionStrings.add("multiplechoicequestion|Which of these is a good city:|Montreal,Trois-Rivieres,Ottawa,Edmonton|Ottawa");
        questionStrings.add("fillblankquestion|Dan is the best teacher for Programming.|best");
        TestGenerator tg = new TestGenerator(questionStrings);

        List<Question> questions = new ArrayList<Question>();
        Question question = new SimpleQuestion("What do eyes do?","provide vision");
        questions.add(question);

        List<String> posAns = new ArrayList<String>();
        posAns.add("Montreal");
        posAns.add("Trois-Rivieres");
        posAns.add("Ottawa");
        posAns.add("Edmonton");
        Question question1 = new MultipleChoiceQuestion("Which of these is a good city:", "Ottawa", posAns);
        questions.add(question1);
        Question question2 = new FillBlankQuestion("Dan is the best teacher for Programming.","best");
        questions.add(question2);
    
        assertEquals(questions, tg.getQuestions());
    }

    @Test
    public void testBadQuestionTypeString(){

        List<String> questionStrings = new ArrayList<String>();
        questionStrings.add("simple|What do eyes do?|provide vision");
        questionStrings.add("multiplechoicequestion|Which of these is a good city:|Montreal,Trois-Rivieres,Ottawa,Edmonton|Ottawa");
        questionStrings.add("fillblankquestion|Dan is the best teacher for Programming.|best");

        try {
            TestGenerator tg = new TestGenerator(questionStrings);
            fail("Should have thrown an exception");
        } catch (IllegalArgumentException e) {
            //TODO: handle exception
        } catch (Exception e) {
            fail("An exception unexpected exception was thrown");
        }
    }
}
